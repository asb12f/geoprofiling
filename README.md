# Geographic Criminal Profiling

This repository contains code for a simple geographic criminal profiling project using R.
The code estimates likely residence for the offender making repeated abduction attempts near campus during the summer of 2015.
Code borrows heavily from code by by [Jeremy Coleman](https://gist.github.com/colemanja91/4184670)